//
//  PresentationWithFade.swift
//  Pods
//
//  Created by IdolEnvy on 11/21/16.
//
//

import Foundation

public class PresentationWithFade : UILabel {
    public func startBlinking() {
        let options : UIViewAnimationOptions = .repeat
        UIView.animate(withDuration: 0.25, delay:0.0, options:options, animations: {
            self.alpha = 0
        }, completion: nil)
    }
    
    public func stopBlinking() {
        alpha = 1
        layer.removeAllAnimations()
    }
}

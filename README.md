# PresentationWithFade

[![CI Status](http://img.shields.io/travis/IdolEnvy/PresentationWithFade.svg?style=flat)](https://travis-ci.org/IdolEnvy/PresentationWithFade)
[![Version](https://img.shields.io/cocoapods/v/PresentationWithFade.svg?style=flat)](http://cocoapods.org/pods/PresentationWithFade)
[![License](https://img.shields.io/cocoapods/l/PresentationWithFade.svg?style=flat)](http://cocoapods.org/pods/PresentationWithFade)
[![Platform](https://img.shields.io/cocoapods/p/PresentationWithFade.svg?style=flat)](http://cocoapods.org/pods/PresentationWithFade)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PresentationWithFade is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PresentationWithFade"
```

## Author

IdolEnvy, detherorc@gmail.com

## License

PresentationWithFade is available under the MIT license. See the LICENSE file for more info.
